import os
from tempfile import mktemp


class AtomicTempFile(object):
    """
    A context manager that will allocate a temporary file in lieu of the desired output file
    on entering. On exiting, the temporary output file will be moved atomically to the desired
    output location.
    """

    def __init__(self, output_target, suffix=None):
        if suffix is None:
            self.suffix = os.path.splitext(output_target)[1]
        else:
            self.suffix = suffix
        self.output_target = os.path.normpath(output_target)
        if os.path.exists(output_target):
            raise Exception("Output target file {} already exists.".format(output_target))

    def __enter__(self):
        self.temp_file = mktemp(dir=os.path.dirname(self.output_target), suffix=self.suffix)
        return self.temp_file

    def __exit__(self, exc_type, exc_value, traceback):
        if exc_type is not None:
            # exception occurred in context
            return False
        if os.path.exists(self.output_target):
            raise Exception("Output target file {} already exists.".format(self.output_target))
        # atomic rename, will throw exception if output_target is a directory that exists
        # if it's a file that exists, it will be overwritten
        os.rename(self.temp_file, self.output_target)
