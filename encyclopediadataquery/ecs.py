"""
ecs.py

This is a modified version of the luigi.contrib.ecs module originally written by Jake Feala for Outlier Bio.
The original code is Copyright 2015 Outlier Bio, LLC
The original code is here: https://github.com/spotify/luigi/blob/master/luigi/contrib/ecs.py (apache 2 license)

Modifications here are Copyright 2017 Jarrett Egertson
"""

import time
import logging
import luigi
import luigi.s3 as s3
import hashlib
import json
import os
from copy import deepcopy
from lakituapi.v0_0.pipeline import LakituAwsConfig

logger = logging.getLogger('luigi-interface')

try:
    import boto3
    client = boto3.client('ecs', region_name=LakituAwsConfig.region)
except ImportError:
    logger.warning('boto3 is not installed. ECSTasks require boto3')

POLL_TIME = 2


class FilePathParameter(luigi.Parameter):
    """
    Reads in a file path from the command line and converts it automatically to an absolute path
    """
    def parse(self, x):
        if x is None:
            return None
        if x.startswith("s3://"):
            return x
        return os.path.abspath(x)


def _get_task_statuses(task_ids, cluster_name):
    """
    Retrieve task statuses from ECS API
    Returns list of {RUNNING|PENDING|STOPPED} for each id in task_ids
    """
    response = client.describe_tasks(cluster=cluster_name, tasks=task_ids)

    # Error checking
    if response['failures'] != []:
        raise Exception('There were some failures:\n{0}'.format(
            response['failures']))
    status_code = response['ResponseMetadata']['HTTPStatusCode']
    if status_code != 200:
        msg = 'Task status request received status code {0}:\n{1}'
        raise Exception(msg.format(status_code, response))

    return [t['lastStatus'] for t in response['tasks']]


def _track_tasks(task_ids, cluster_name):
    """Poll task status until STOPPED"""
    while True:
        statuses = _get_task_statuses(task_ids=task_ids, cluster_name=cluster_name)
        if all([status == 'STOPPED' for status in statuses]):
            logger.info('ECS tasks {0} STOPPED'.format(','.join(task_ids)))
            break
        time.sleep(POLL_TIME)
        logger.debug('ECS task status for tasks {0}: {1}'.format(
            ','.join(task_ids), status))


def _get_task_def_if_exists(family):
    """
    Get the most recent active task definition for the family if it exists
    :param family: name of the task family
    :return: task definition arn if task exists, otherwise none
    """
    response = client.list_task_definitions(familyPrefix=family,
                                            status='ACTIVE',
                                            sort='DESC',
                                            maxResults=1)

    if len(response['taskDefinitionArns']) == 0:
        return None
    return response['taskDefinitionArns'][0]


def _get_or_register_task_definition(l_task_def, force_registration=False):
    """
    Register a task definition the ECS cluster if an active version of it does not already exist
    (determined by family name)
    :param l_task_def: task definition dictionary suitable for boto3 ecs client register_task_definition function call
    :param force_registration: if True, task will be registered even if it is determined to already exist
    :return: task definition arn
    """

    task_def = deepcopy(l_task_def)
    # the task definition is hashed to keep from submitting a new revision every time a task is run
    task_def['family'] = l_task_def['family'] + '_' + _hash_task_def(l_task_def)

    if force_registration:
        logging.info("Forced registration for task family {}".format(task_def['family']))
        task_def_arn = client.register_task_definition(**task_def)['taskDefinition']['taskDefinitionArn']
    else:
        # returns None if task def doesn't exist already
        task_def_arn = _get_task_def_if_exists(task_def['family'])

    # if there are no task definition with the same family name, this task must be registered
    if task_def_arn is None:
        logging.info("No tasks for family {} found, registering new task".format(task_def['family']))
        task_def_arn = client.register_task_definition(**task_def)['taskDefinition']['taskDefinitionArn']
    return task_def_arn


def _hash_task_def(task_def):
    # hash a task definition by converting to json and calculating md5
    return hashlib.md5(json.dumps(task_def, sort_keys=True).encode('utf-8')).hexdigest()


class ECSTask(luigi.Task):
    """
    Base class for an Amazon EC2 Container Service Task
    Amazon ECS requires you to register "tasks", which are JSON descriptions
    for how to issue the ``docker run`` command. This Luigi Task can either
    run a pre-registered ECS taskDefinition, OR register the task on the fly
    from a Python dict.
    :param task_def_arn: pre-registered task definition ARN (Amazon Resource
        Name), of the form::
            arn:aws:ecs:<region>:<user_id>:task-definition/<family>:<tag>
    :param task_def: dict describing task in taskDefinition JSON format, for
        example::
            task_def = {
                'family': 'hello-world',
                'volumes': [],
                'containerDefinitions': [
                    {
                        'memory': 1,
                        'essential': True,
                        'name': 'hello-world',
                        'image': 'ubuntu',
                        'command': ['/bin/echo', 'hello world']
                    }
                ]
            }
    """

    task_def_arn = luigi.Parameter(default="")
    task_def = luigi.DictParameter(default=None)
    cluster_name = luigi.Parameter(default="default")
    container_name = luigi.Parameter(default=None)
    command_base = []

    @property
    def ecs_task_ids(self):
        """Expose the ECS task ID"""
        if hasattr(self, '_task_ids'):
            return self._task_ids

    @property
    def parameters(self):
        """
        Parameters to pass to the command template

        Override to return a dict of key-value pairs to fill in command arguments
        :return:
        """
        return {}

    @property
    def environment(self):
        """
        Environment variables to set in docker container when command is run

        Override to return a list of key-value pairs. dicts of this format:
        {'name': var_name, 'value': var_value}
        see
        http://boto3.readthedocs.io/en/latest/reference/services/ecs.html#ECS.Client.run_task
        :return:
        """
        return []

    @property
    def downloadable_outputs(self):
        """
        :return: a listing of s3 outputs from this task that should be downloaded from s3 to the system that spawned
        this pipeline run
        """
        o = self.output()
        if isinstance(o, s3.S3Target):
            return [o]
        if isinstance(o, list):
            return [t for t in o if isinstance(t, s3.S3Target)]
        if isinstance(o, dict):
            return [t for t in o.values() if isinstance(t, s3.S3Target)]
        return list()

    def command(self, param_overrides=None):
        """
        Command passed to the containers, derived from command_base with
        replacement of parameters (Ref::name_param) with their value derived
        from the parameters property
        Directly corresponds to the `overrides` parameter of runTask API. For
        example::
            [
                {
                    'name': 'myContainer',
                    'command': ['/bin/sleep', '60']
                }
            ]
        """
        def get_param(arg):
            k = arg.split('::')[1]
            if param_overrides is not None:
                for param_name in param_overrides.keys():
                    if param_name not in self.parameters:
                        raise RuntimeWarning("Tried to override a parameter ({})"
                                             "that is not present in task params.".format(param_name))

            if param_overrides is not None and k in param_overrides:
                return param_overrides[k]
            else:
                return self.parameters[k]
        cmd_args = [get_param(arg) if arg.startswith('Ref::') else arg for arg in self.command_base]
        return cmd_args

    def run(self):
        # override this if you want to add other stuff after run_ecs
        self.run_ecs()

    def run_ecs(self, param_overrides=None):
        if self.task_def is None and (not self.task_def_arn):
            raise ValueError('Neither task_def nor task_def_arn are assigned')
        if (self.task_def is not None) and self.task_def_arn:
            raise ValueError('Both task_def and task_def_arn are assigned. Must have only one assigned.')
        if not self.task_def_arn:
            # Register the task and get assigned taskDefinition ID (arn)
            self.task_def_arn = _get_or_register_task_definition(self.task_def)

        # Submit the task to AWS ECS and get assigned task ID
        # (list containing 1 string)
        command_override = self.command(param_overrides=param_overrides)
        overrides = {}
        if len(command_override) > 0:
            if 'containerOverrides' not in overrides:
                overrides = {'containerOverrides': [{'name': self.container_name}]}
            overrides['containerOverrides'][0]['command'] = command_override

        # set any environment variables requested by the task
        if len(self.environment) > 0:
            if 'containerOverrides' not in overrides:
                overrides = {'containerOverrides': [{'name': self.container_name}]}
            overrides['containerOverrides'][0]['environment'] = self.environment
        # TODO: total length of overrides cannot exceed 8192 characters see
        # http://boto3.readthedocs.io/en/latest/reference/services/ecs.html#ECS.Client.run_task
        retry_count = 0
        self._task_ids = list()
        while retry_count < 5:
            response = client.run_task(cluster=self.cluster_name,
                                       taskDefinition=self.task_def_arn,
                                       overrides=overrides)
            self._task_ids = [task['taskArn'] for task in response['tasks']]
            if len(self._task_ids) > 0:
                break
            time.sleep(5)
            retry_count += 1

        if len(self._task_ids) == 0:
            print response
        # Wait on task completion
        _track_tasks(task_ids=self._task_ids, cluster_name=self.cluster_name)


