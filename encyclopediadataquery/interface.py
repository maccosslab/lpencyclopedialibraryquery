from querypipeline import *
from luigi.tools import deps


@inherits(EncyclopediaParams)
class MainTask(luigi.WrapperTask):
    ms_set = FilePathParameter(description="Path to a local folder containing MS files or a file listing "
                                            "paths to local or S3 MS files")
    output_path = FilePathParameter(description="Path to download processing output to")

    def requires(self):
        rl = self.clone(RetentionateLibrary)
        yield rl
        yield DownloadAll(seed_task=rl, output_path=self.output_path)


class DownloadAll(luigi.WrapperTask):
    seed_task = SpecificTaskParameter()
    output_path = FilePathParameter()

    def requires(self):
        upstream = deps.find_deps(self.seed_task, None)
        for t in upstream:
            if not isinstance(t, ECSTask):
                continue
            if len(t.downloadable_outputs) > 0:
                yield DownloadOutputs(task_object=t, output_path=self.output_path)


if __name__ == '__main__':
    # logging.getLogger('encyclopedia-task').addHandler(h)
    luigi.run()
