import luigi
from luigi.util import inherits
from lakituapi.v0_0.pipeline import LakituAwsConfig
from datetime import datetime
import luigi.s3 as s3
import os
import s3helpers
import fshelpers
from ecs import ECSTask, FilePathParameter
from tempfile import mkstemp


def get_log_config():
    """Get the logging configuration for an aws task definition"""
    return {'logDriver': 'awslogs',
            'options': {'awslogs-group': LakituAwsConfig.log_group_name,
                        'awslogs-region': LakituAwsConfig.log_group_region,
                        'awslogs-stream-prefix': "edq"}}


class BuildWorkingDirectory(luigi.Task):
    """
    Build a working directory for the pipeline
    """
    s3_bucket = luigi.Parameter(default=LakituAwsConfig.s3_run_bucket,
                                description="The root s3 processing directory")
    run_id = luigi.Parameter(default=datetime.now().strftime('%Y-%m-%d_%H-%M-%S'))

    def run(self):
        s3helpers.mkdir_s3(self.output().path)

    def output(self):
        return s3.S3Target(os.path.join("s3://", self.s3_bucket,
                                        "edq_runs",
                                        "edq_{}".format(self.run_id)) + '/')


class BuildToolDirectory(luigi.Task):
    """
    Build a working directory for a tool. Sub directories can be defined using ::
    E.g. to put a tool directory for elib_boundaries under sub directory post processing
    pass toolname as postprocessing::elib_boundaries
    """
    toolname = luigi.Parameter()

    def requires(self):
        return {'base_directory': BuildWorkingDirectory()}

    def run(self):
        s3helpers.mkdir_s3(self.output().path)

    def output(self):
        base_path = self.input()['base_directory'].path
        return s3.S3Target(os.path.join(base_path, *(self.toolname.split('::'))))


class UploadMsFileToS3(luigi.Task):
    f_path = FilePathParameter()

    @property
    def _needs_upload(self):
        return not self.f_path.startswith('s3://')

    @property
    def resources(self):
        if self._needs_upload:
            # allow only one raw upload at a time per lakitu working directory
            stamp = self.requires()['s3_wd_location'].run_id
            return {"large_upload_{}".format(stamp): 1}
        return {}

    def requires(self):
        return {"s3_wd_location": BuildWorkingDirectory(),
                "msfiles_dir": BuildToolDirectory(toolname="msfiles")}

    def run(self):
        if self._needs_upload:
            s3helpers.upload_to_s3(self.f_path, self.output().path, show_progress=True)

    def output(self):
        if not self._needs_upload:
            return s3.S3Target(self.f_path)
        raw_name = os.path.basename(self.f_path)
        file_subfolder = os.path.splitext(raw_name)[-1][1:].lower()
        return s3.S3Target(os.path.join(self.input()['msfiles_dir'].path, file_subfolder, raw_name))


class UploadLibraryFileToS3(luigi.Task):
    local_file = FilePathParameter()

    @property
    def resources(self):
        # allow only one large upload at a time per lakitu working directory
        stamp = self.requires()['s3_wd_location'].run_id
        return {"large_upload_{}".format(stamp): 1}

    def requires(self):
        return {"s3_wd_location": BuildWorkingDirectory()}

    def run(self):
        s3helpers.upload_to_s3(self.local_file, self.output().path, show_progress=True)

    def output(self):
        local_name = os.path.basename(self.local_file)
        return s3.S3Target(os.path.join(self.input()['s3_wd_location'].path, local_name))


class UploadMSConvertConfig(luigi.Task):
    msconvert_config = FilePathParameter(description="Path (s3 or local) to msconvert config")

    def requires(self):
        return {"msfiles_dir": BuildToolDirectory('msfiles')}

    def run(self):
        if not self.msconvert_config.startswith('s3://'):
            s3helpers.upload_to_s3(self.msconvert_config, self.output().path)

    def output(self):
        if self.msconvert_config.startswith('s3://'):
            return s3.S3Target(self.msconvert_config)
        return s3.S3Target(os.path.join(self.input()['msfiles_dir'].path, os.path.basename(self.msconvert_config)))


class MSConvert(ECSTask):
    download_converted = luigi.BoolParameter(significant=False, default=False, description="Download msconvert outputs"
                                                                                           "to client disk?")
    bypass_conversion = luigi.BoolParameter(default=True, description="If true, file conversion is bypassed if"
                                                                      "the input file extension matches the desired"
                                                                      " extension.")
    msc_in_path = FilePathParameter(description="S3 or local location for input file to convert")
    msc_out_format = luigi.ChoiceParameter(choices=['mzML', 'mzXML', 'mz5', 'mgf',
                                                    'text', 'ms1', 'cms1', 'ms2', 'cms2'],
                                           default='mzML',
                                           description="Desired output format for file, maps to "
                                                       "proteowizard types")

    msc_config = FilePathParameter(default=None, description="Path (s3 or local) to msconvert config")
    container_name = "msconvert"
    cluster_name = LakituAwsConfig.windows_cluster_name
    task_def = {
        'family': 'msconvert',
        'taskRoleArn': LakituAwsConfig.task_arn,
        'volumes': [],
        'containerDefinitions': [
            {
                'name': container_name,
                'image': 'jegertso/msconvert:3.0.11579-1',
                'memory': 2048,
                'cpu': 1024,
                'logConfiguration': get_log_config(),
            }
        ]
    }

    @property
    def _needs_convert(self):
        file_ext = os.path.splitext(self.msc_in_path)[-1][1:]
        return not (self.bypass_conversion and self.msc_out_format.lower() == file_ext.lower())

    def requires(self):
        reqs = {"msfiles_dir": BuildToolDirectory('msfiles'),
                "msc_in": UploadMsFileToS3(f_path=self.msc_in_path),
                "msconvert_dir": BuildToolDirectory(toolname="msconvert")}
        if self.msc_config is not None:
            reqs['msc_config_p'] = UploadMSConvertConfig(msconvert_config=self.msc_config)
        return reqs

    def run(self):
        if self._needs_convert:
            self.command_base = ['Ref::in_path', '-o', 'Ref::out_dir', '--outfile', 'Ref::out_path', 'Ref::extension']
            if self.msc_config is not None:
                # get the msconvert config
                self.command_base.extend(['-c', self.input()['msc_config_p'].path])
            self.run_ecs()

    @property
    def parameters(self):
        return {
            'in_path': self.input()['msc_in'].path,
            'out_dir': os.path.dirname(self.output().path),
            'out_path': self.output().path,
            'extension': '--' + self.msc_out_format
        }

    def output(self):
        if self._needs_convert:
            f_bname = os.path.basename(self.input()['msc_in'].path)
            f_out_name = os.path.splitext(f_bname)[0] + '.' + self.msc_out_format
            return s3.S3Target(os.path.join(self.input()['msconvert_dir'].path, f_out_name))
        else:
            return s3.S3Target(self.input()['msc_in'].path)

    @property
    def downloadable_outputs(self):
        if self.download_converted and self._needs_convert:
            return [self.output()]
        else:
            return []


class EncyclopediaParams(luigi.Config):
    precursor_ppm_error = luigi.FloatParameter(default=10.0,
                                               description='Encyclopedia: precursor ppm error')
    fragment_ppm_error = luigi.FloatParameter(default=10.0,
                                              description='Encyclopedia: fragment ppm error')
    query_elib_file = FilePathParameter(description='Encyclopedia elib library to query dia data with')


@inherits(EncyclopediaParams)
class EncyclopediaSingle(ECSTask):
    ms_file = FilePathParameter()
    container_name = "encyclopedia"
    cluster_name = LakituAwsConfig.linux_cluster_name
    task_def = {
        'family': 'encyclopedia_0_4_4',
        'volumes': [],
        'containerDefinitions': [
            {
                'name': container_name,
                'image': 'jegertso/encyclopedia:0.4.4-1',
                'memoryReservation': 6500,
                'cpu': 1024,     # allocate at least one core for this
                'logConfiguration': get_log_config(),
            }
        ]
    }

    command_base = ['Ref::memory',
                    '-acquisition', 'DIA',
                    '-i', 'Ref::mzml_in',
                    '-l', 'Ref::elib_in',
                    '-ptol', 'Ref::prec_ppm',
                    '-ftol', 'Ref::frag_ppm']

    @property
    def environment(self):
        additional_uploads = ""
        # get the mzml prefix: e.g. s3://bucket_name/mzml_dir/mzml_file.mzml -> bucket_name/mzml_dir/mzml_file
        mzml_prefix = os.path.splitext(self.input()['mzml_s3'].path)[0][5:]
        additional_uploads += mzml_prefix + '.dia'
        additional_uploads += ':' + mzml_prefix + '.mzML.elib'
        additional_uploads += ':' + mzml_prefix + '.mzML.encyclopedia.txt'
        additional_uploads += ':' + mzml_prefix + '.mzML.encyclopedia.txt.delta_rt.pdf'
        additional_uploads += ':' + mzml_prefix + '.mzML.encyclopedia.txt.log'
        additional_uploads += ':' + mzml_prefix + '.mzML.encyclopedia.txt.rt_fit.pdf'
        additional_uploads += ':' + mzml_prefix + '.mzML.encyclopedia.txt.rt_fit.txt'
        additional_uploads += ':' + mzml_prefix + '.mzML.features.txt'
        additional_uploads += ':' + mzml_prefix + '.mzML.first_round.txt'

        return [{'name': 's3wrap_extra_uploads', 'value': additional_uploads}]

    @property
    def parameters(self):
        return {
            'memory': '-Xmx6g',
            'mzml_in': self.input()['mzml_s3'].path,
            'elib_in': self.input()['library_s3'].path,
            'prec_ppm': str(self.precursor_ppm_error),
            'frag_ppm': str(self.fragment_ppm_error)
        }

    def requires(self):
        return {'s3_wd_location': BuildWorkingDirectory(),
                'tool_wd': BuildToolDirectory(toolname='encyclopedia'),
                'library_s3': UploadLibraryFileToS3(local_file=self.query_elib_file),
                'mzml_s3': MSConvert(msc_in_path=self.ms_file, msc_out_format='mzML')}

    def run(self):
        self.run_ecs()
        # copy output from the input raw file directory to the correct output directory
        # try to get all files that share the prefix of the input file (with .mzml removed)
        # raw_s3 path: 's3://bucket-name/path/to/input_file.mzML
        # mzml_in_prefix: 's3://bucket-name/path/to/input_file'

        mzml_in_prefix = os.path.splitext(self.input()['mzml_s3'].path)[0]

        # get objects with the same prefix key in this bucket
        for prefix_match in s3helpers.search_s3_prefix(mzml_in_prefix):
            # build the path where the file should be put
            mzml_path = os.path.dirname(mzml_in_prefix)
            match_suffix = prefix_match[len(mzml_path):]
            if match_suffix.startswith('/'):
                match_suffix = match_suffix[1:]
            output_path = os.path.join(self.output()['output_directory'].path, match_suffix)

            if prefix_match == self.input()['mzml_s3'].path:
                # copy the input mzml
                print "Copying {} -> {}".format(prefix_match, output_path)
                s3helpers.cp_s3(prefix_match, output_path)
            else:
                print "Moving {} -> {}".format(prefix_match, output_path)
                s3helpers.mv_s3(prefix_match, output_path)

    def output(self):
        out_dir = self.input()['tool_wd'].path
        mzml_basename = os.path.basename(self.input()['mzml_s3'].path)
        mzml_prefix = os.path.splitext(mzml_basename)[0]
        return {'output_directory': s3.S3Target(out_dir),
                'elib': s3.S3Target(os.path.join(out_dir, mzml_basename + '.elib')),
                'dia': s3.S3Target(os.path.join(out_dir, mzml_prefix + '.dia')),
                'encyc_txt': s3.S3Target(os.path.join(out_dir, mzml_basename + '.encyclopedia.txt')),
                'features_txt': s3.S3Target(os.path.join(out_dir, mzml_basename + '.features.txt')),
                'copied_mzml': s3.S3Target(os.path.join(out_dir))
                }

    @property
    def downloadable_outputs(self):
        o = self.output()
        return [t for k, t in o.iteritems() if k not in ('output_directory', 'copied_mzml')]


@inherits(EncyclopediaParams)
class EncyclopediaBuildMergedLib(ECSTask):
    # path to directory containing mzml files to analyze
    ms_set = FilePathParameter(description="Folder location of ms files to analyze on local system, or a text file"
                                           " containing local and/or s3 paths")
    # perform rt alignment?
    rt_align = luigi.ChoiceParameter(choices=['true', 'false'], var_type=str, default='true',
                                     description='Encyclopedia: perform retention time alignment?')
    # output blib instead of elib?
    blib = luigi.ChoiceParameter(choices=['true', 'false'], var_type=str, default='false',
                                 description="If true, output blib file, if false (default) output elib")
    out_lib_name = luigi.Parameter(default='merged')

    container_name = "encyclopedia_merge"
    cluster_name = LakituAwsConfig.linux_cluster_name
    task_def = {
        'family': 'encyclopedia_0_4_4_mergelib',
        'volumes': [],
        'containerDefinitions': [
            {
                'name': container_name,
                'image': 'jegertso/encyclopedia:0.4.4-1',
                'memoryReservation': 6500,
                'cpu': 1024,    # allocate at least one core for merging
                'logConfiguration': get_log_config()
            }
        ]
    }

    command_base = ['Ref::memory',
                    '-libexport',
                    '-i', 'Ref::in_directory',
                    '-o', 'Ref::out_library',
                    '-l', 'Ref::query_elib',
                    '-a', 'Ref::rt_align',
                    '-blib', 'Ref::blib']

    @property
    def parameters(self):
        return {
            'memory': '-Xmx6g',
            'in_directory': self.encyclopedia_s3_dir,
            'out_library': self.output().path,
            'query_elib': self.query_elib_s3,
            'rt_align': self.rt_align,
            'blib': self.blib
        }

    @property
    def lib_suffix(self):
        return '.blib' if self.blib == 'true' else '.elib'

    @property
    def encyclopedia_s3_dir(self):
        # directory with encyclopedia outputs
        e_path = self.input().values()[0]['output_directory'].path
        if not e_path.endswith('/'):
            e_path += '/'
        return e_path

    @property
    def query_elib_s3(self):
        # location on s3 of the query elib file
        return self.requires().values()[0].input()['library_s3'].path

    def _get_ms_paths(self):
        if not os.path.exists(self.ms_set):
            raise ValueError("ms file path {} does not exist".format(self.ms_set))
        if os.path.isdir(self.ms_set):
            formats = ("mzml", "mzxml", "mz5", "mgf", "text", "ms2", "cms2", "raw", "d", "wiff")
            return [os.path.abspath(os.path.join(self.ms_set, f))
                    for f in os.listdir(self.ms_set) if os.path.splitext(f)[-1][1:].lower() in formats]
        # path is a text file with an mzml file path on each line
        with open(self.ms_set) as fin:
            return [line.strip() for line in fin.readlines()]

    def requires(self):
        # requires encyclopedia single run on all input mzml files
        return {ms_file: self.clone(EncyclopediaSingle, ms_file=ms_file)
                for ms_file in self._get_ms_paths()}

    def output(self):
        return s3.S3Target(os.path.join(self.encyclopedia_s3_dir, self.out_lib_name) + self.lib_suffix)


@inherits(EncyclopediaBuildMergedLib)
class ExtractElibBoundaries(ECSTask):
    out_boundaries_name = luigi.Parameter(default='elib_boundaries')

    container_name = 'extract_elib_boundaries'
    cluster_name = LakituAwsConfig.linux_cluster_name
    task_def = {
        'family': 'diapipeline_0_1_1_extract_elib_boundaries',
        'volumes': [],
        'containerDefinitions': [
            {
                'name': container_name,
                'image': 'jegertso/diapipeline:0.1.1-1',
                'memoryReservation': 1024,  # require 1 Gb of memory
                'cpu': 128,     # allocate 1/8th of a cpu core
                'logConfiguration': get_log_config()
            }
        ]
    }

    command_base = ['ElibBoundaries',
                    '--overwrite',
                    'Ref::in_elib',
                    'Ref::out_boundaries']

    @property
    def parameters(self):
        return {
            'in_elib': self.input()['input_elib'].path,
            'out_boundaries': self.output().path
        }

    def requires(self):
        return {'input_elib': self.clone(EncyclopediaBuildMergedLib),
                'tool_wd': BuildToolDirectory(toolname="postprocessing::elib_boundaries")}

    def output(self):
        # extracted boundaries
        output_path = os.path.join(self.input()['tool_wd'].path, self.out_boundaries_name) + '.csv'
        return s3.S3Target(output_path)


@inherits(EncyclopediaBuildMergedLib)
class IntegrateBoundaries(ECSTask):
    integrated_boundaries_name = luigi.Parameter(default='elib_boundaries_integrated')

    container_name = 'integrate_boundaries'
    cluster_name = LakituAwsConfig.linux_cluster_name
    task_def = {
        'family': 'diapipeline_0_1_1_integrate_boundaries',
        'volumes': [],
        'containerDefinitions': [
            {
                'name': container_name,
                'image': 'jegertso/diapipeline:0.1.1-1',
                'memoryReservation': 6500,  # require 6.5 Gb of memory
                'cpu': 1024,     # require at least a full cpu core
                'logConfiguration': get_log_config()
            }
        ]
    }

    command_base = ['TheIntegrator',
                    '--writePlots',
                    '--removeOutliers',
                    '--impute',
                    'Ref::in_boundaries_list',
                    'Ref::out_boundaries']

    @property
    def parameters(self):
        return {
            'in_boundaries_list': self.boundary_list_path,
            'out_boundaries': self.output().path
        }

    @property
    def environment(self):
        # s3wrap_file_lists
        return [{'name': 's3wrap_file_lists', 'value': self.boundary_list_path[5:]}]

    @property
    def boundary_list_path(self):
        return os.path.join(self.input()['tool_wd'].path, 'list_of_boundaries.txt')

    def run(self):
        # create a file containing the input boundary file paths and upload it
        fd, temp_path = mkstemp()
        with os.fdopen(fd, 'w') as fout:
            fout.write(self.input()['input_boundaries'].path)
        s3helpers.upload_to_s3(temp_path, self.boundary_list_path, show_progress=False)
        os.remove(temp_path)
        self.run_ecs()

    def requires(self):
        return {
            'input_boundaries': self.clone(ExtractElibBoundaries),
            'tool_wd': BuildToolDirectory(toolname='postprocessing::boundary_integration')
        }

    def output(self):
        output_path = os.path.join(self.input()['tool_wd'].path, self.integrated_boundaries_name) + '.csv'
        return s3.S3Target(output_path)


@inherits(EncyclopediaBuildMergedLib)
class DesearleinateLibrary(ECSTask):
    # luigi parameters
    desearleinated_library_name = luigi.Parameter(default='desearleinated')

    container_name = 'blib_desearleinate'
    cluster_name = LakituAwsConfig.linux_cluster_name
    task_def = {
        'family': 'diapipeline_0_1_1_blib_desearleinate',
        'volumes': [],
        'containerDefinitions': [
            {
                'name': container_name,
                'image': 'jegertso/diapipeline:0.1.1-1',
                'memoryReservation': 5000,
                'cpu': 1024,
                'logConfiguration': get_log_config()
            }
        ]
    }

    command_base = ['BlibDesearleinator',
                    'Ref::in_blib',
                    'Ref::out_blib']

    @property
    def parameters(self):
        return {
            'in_blib': self.input()['in_blib'].path,
            'out_blib': self.output().path
        }

    def requires(self):
        return {
            'tool_wd': BuildToolDirectory(toolname='postprocessing::blib_desearle'),
            'in_blib': self.clone(EncyclopediaBuildMergedLib, blib='true')
        }

    def output(self):
        output_path = os.path.join(self.input()['tool_wd'].path, self.desearleinated_library_name) + '.blib'
        return s3.S3Target(output_path)


@inherits(EncyclopediaBuildMergedLib)
class RetentionateLibrary(ECSTask):
    # luigi parameters
    retentionated_library_name = luigi.Parameter(default='retentionated')

    container_name = 'blib_retentionate'
    cluster_name = LakituAwsConfig.linux_cluster_name
    task_def = {
        'family': 'diapipeline_0_1_1_blib_retentionate',
        'volumes': [],
        'containerDefinitions': [
            {
                'name': container_name,
                'image': 'jegertso/diapipeline:0.1.1-1',
                'memoryReservation': 5000,
                'cpu': 1024,
                'logConfiguration': get_log_config()
            }
        ]
    }

    command_base = ['BlibRetentionator',
                    '--addNew',
                    'Ref::in_blib',
                    'Ref::in_boundaries_list',
                    'Ref::out_blib']

    @property
    def parameters(self):
        return {
            'in_blib': self.input()['input_blib'].path,
            'in_boundaries_list': self.boundary_list_path,
            'out_blib': self.output().path
        }

    @property
    def environment(self):
        # s3wrap_file_lists
        return [{'name': 's3wrap_file_lists', 'value': self.boundary_list_path[5:]}]

    @property
    def boundary_list_path(self):
        return os.path.join(self.input()['tool_wd'].path, 'list_of_boundaries.txt')

    def run(self):
        # create a file containing the input boundary file paths and upload it
        fd, temp_path = mkstemp()
        with os.fdopen(fd, 'w') as fout:
            fout.write(self.input()['input_boundaries'].path)
        s3helpers.upload_to_s3(temp_path, self.boundary_list_path, show_progress=False)
        os.remove(temp_path)
        self.run_ecs()

    def requires(self):
        return {
            'tool_wd': BuildToolDirectory(toolname='postprocessing::blib_retentionate'),
            'input_boundaries': self.clone(IntegrateBoundaries),
            'input_blib': self.clone(DesearleinateLibrary)
        }

    def output(self):
        output_path = os.path.join(self.input()['tool_wd'].path, self.retentionated_library_name + '.blib')
        return s3.S3Target(output_path)


class SpecificTaskParameter(luigi.Parameter):
    """
    Parameter corresponding to a specific instantiated task object
    """
    def serialize(self, x):
        return x.task_id


class DownloadOutputs(luigi.Task):
    task_object = SpecificTaskParameter()
    output_path = FilePathParameter()

    @property
    def resources(self):
        # allow only one download at a time per lakitu working directory
        stamp = self.requires()['base_directory'].run_id
        return {"download_{}".format(stamp): 1}

    def requires(self):
        return {'task': self.task_object,
                'base_directory': BuildWorkingDirectory()}

    def run(self):
        # do transfer to temp location and then move (use luigi thing)
        for s3_path, local_target in self.output().iteritems():
            local_path = local_target.path
            out_dir = os.path.dirname(local_path)
            if not os.path.exists(out_dir):
                os.makedirs(out_dir)
            with fshelpers.AtomicTempFile(local_path) as temp_output_path:
                s3helpers.download_from_s3(s3_path, temp_output_path, show_progress=False)

    def output(self):
        # map the s3 locations to local file paths, removing suffix of s3 base dir
        dl_requests = dict()
        for s3_target in self.requires()['task'].downloadable_outputs:
            s3_path = s3_target.path
            path_suffix = os.path.relpath(s3_path, start=self.input()['base_directory'].path)
            out_path = os.path.join(self.output_path, path_suffix)
            dl_requests[s3_path] = luigi.LocalTarget(out_path)
        return dl_requests

