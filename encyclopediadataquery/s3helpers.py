from urlparse import urlsplit
import os
import boto3
from boto3.s3.transfer import S3Transfer
import threading
import sys
from lakituapi.v0_0.pipeline import LakituAwsConfig

bs3 = boto3.client("s3", region_name=LakituAwsConfig.region)
bs3r = boto3.resource("s3", region_name=LakituAwsConfig.region)
s3transfer = S3Transfer(bs3)


class ProgressPercentage(object):
    def __init__(self, filename):
        self._filename = filename
        self._size = float(os.path.getsize(filename))
        self._seen_so_far = 0
        self._lock = threading.Lock()

    def __call__(self, bytes_amount):
        with self._lock:
            self._seen_so_far += bytes_amount
            percentage = (self._seen_so_far / self._size) * 100
            sys.stdout.write(
                "\rUploading %s  %s / %s  (%.2f%%)" % (
                    os.path.basename(self._filename), self._seen_so_far, self._size, percentage)
                )
            sys.stdout.flush()


def bucket_and_key_from_s3(input_path):
    """Convert a path like s3://lakitu-data/here/is/a/path/blah.txt to bucket , key:
    bucket: lakitu-data key: here/is/a/path/blah.txt"""
    (_, bucket, key, _, _) = urlsplit(input_path)
    return bucket, key[1:]


def bucket_and_key_to_s3(bucket, key):
    return os.path.join("s3://", bucket, key)


def upload_to_s3(local_file, s3_path, show_progress=False):
    """
    wrapper around s3transfer to use s3 path "s3://bucket-name/here/is/a/file.txt"
    rather than bucket, key arguments: bucket=bucket-name , key=here/is/a/file.txt
    :param local_file: file path to file on local system
    :param s3_path: s3 location as described above
    :param show_progress: set to True to show file upload progress
    :return:
    """
    bucket, key = bucket_and_key_from_s3(s3_path)
    callback = None
    if show_progress:
        callback = ProgressPercentage(local_file)
    s3transfer.upload_file(local_file, bucket=bucket, key=key, callback=callback)


def download_from_s3(s3_path, local_file, show_progress=False):
    """
    wrapper around s3transfer to use s3 path "s3://bucket-name/here/is/a/file.txt"
    rather than bucket, key arguments: bucket=bucket-name , key=here/is/a/file.txt
    :param s3_path: s3 location as described above
    :param local_file: file path to file on local system
    :param show_progress: set to True to show file upload progress
    :return:
    """
    bucket, key = bucket_and_key_from_s3(s3_path)
    callback = None
    if show_progress:
        callback = ProgressPercentage(local_file)
    s3transfer.download_file(bucket=bucket, key=key, filename=local_file, callback=callback)


def mkdir_s3(s3_path):
    bucket, key = bucket_and_key_from_s3(s3_path)
    if not key[-1] == '/':
        key += '/'
    bs3.put_object(Bucket=bucket, Key=key)


def touch_s3(s3_file):
    bucket, key = bucket_and_key_from_s3(s3_file)
    bs3.put_object(Bucket=bucket, Key=key)


def search_s3_prefix(s3_path_prefix):
    """
    Find objects with the same prefix as s3_path_prefix
    :param s3_path_prefix: prefix e.g. s3://bucket-name/path/to/file
    :yields s3 paths for objects matching the prefix
    """
    bucket, key_prefix = bucket_and_key_from_s3(s3_path_prefix)
    paginator = bs3.get_paginator('list_objects_v2')
    page_iterator = paginator.paginate(Bucket=bucket,
                                       Prefix=key_prefix)
    for page in page_iterator:
        for result in page['Contents']:
            result_key = result['Key']
            yield bucket_and_key_to_s3(bucket, result_key)


def mv_s3(source_path, destination_path):
    src_bucket, src_key = bucket_and_key_from_s3(source_path)
    dst_bucket, dst_key = bucket_and_key_from_s3(destination_path)
    try:
        bs3r.Object(dst_bucket, dst_key).copy_from(CopySource={'Bucket': src_bucket, 'Key': src_key})
    except:
        raise
    else:
        bs3r.Object(src_bucket, src_key).delete()


def cp_s3(source_path, destination_path):
    src_bucket, src_key = bucket_and_key_from_s3(source_path)
    dst_bucket, dst_key = bucket_and_key_from_s3(destination_path)
    bs3r.Object(dst_bucket, dst_key).copy_from(CopySource={'Bucket': src_bucket, 'Key': src_key})
