from setuptools import setup, find_packages
from subprocess import check_output


def get_version_string():
    output = check_output(["git", "describe", "--tags"])
    parts = output.split('-')
    tag, count, sha = parts[:3]
    return "{}.dev{}+{}".format(tag, count, sha)

setup(
    name="encyclopediadataquery",
    version=get_version_string(),
    description="A DIA data query pipeline",
    author="Jarrett Egertson",
    author_email="jegertso@uw.edu",
    classifiers=['Development Status :: 3 - Alpha',
                 'Programming Language :: Python :: 2.7'],
    packages=find_packages(),
    install_requires=['luigi', 'boto3', 'lakituapi', 'boto']
)
